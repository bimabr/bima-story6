from django import forms
from .models import Kegiatan, Person


class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['title','desc','date']
        
    my_widget = forms.TextInput(attrs={'class':'form-control', 'spellcheck':'false'})
    title = forms.CharField(widget=my_widget, max_length=30)
    desc = forms.CharField(widget=my_widget, max_length=255)
    date_widget = forms.DateTimeInput(
        attrs= {
            'class' : 'form-control datetimepicker-input',
            'data-target' :'#datetimepicker1'
        }
    )
    date = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'], widget=forms.DateTimeInput()) 