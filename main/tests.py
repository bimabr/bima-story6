from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse, resolve
from django.apps import apps
from main.apps import MainConfig
# from selenium import webdriver
from .models import Kegiatan, Person
from .views import home, add_kegiatan
from .forms import KegiatanForm
from datetime import datetime
import pytz

class MainTestCase(TestCase):
    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main')
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_using_add_kegiatan_func(self):
        found = resolve('/add_kegiatan/')
        self.assertEqual(found.func, add_kegiatan)

    def test_model_can_create_kegiatan(self):
        d = datetime(2021, 1, 1, 0, 0, tzinfo=pytz.UTC)
        new_kegiatan = Kegiatan.objects.create(
            title="Tahun baru", date=d, desc="Berkumpul di Pacil"
        )
        count_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(count_kegiatan, 1)

    def test_model_can_create_person(self):
        new_person = Person.objects.create(nama="Kak Pewe")
        count_person = Person.objects.all().count()
        self.assertEqual(count_person, 1)

    def test_can_save_a_POST_request(self):
        data = {
            'title' : 'Tahun Baru 2021',
            'date' : '1/1/2021 00:00',
            'desc' : 'Merayakan tahun baru bersama-sama'
        }
        response = self.client.post('/add_kegiatan/', data=data)
        count_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(count_kegiatan, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Tahun Baru 2021', html_response)

    def test_form_is_valid_for_blank_items(self):
        form = KegiatanForm(data={
            'title' : '', 'desc':'', 'date':'ASKDOKWQODWO'
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['desc'], ["This field is required."]
        )
        self.assertEqual(
            form.errors['date'], ['Enter a valid date/time.']
        )

    def test_add_person_to_kegiatan(self):
        kegiatan = Kegiatan(
            title = 'Tahun Baru 2021',
            date = '2021-1-1 00:00',
            desc = 'Merayakan tahun baru bersama-sama'
        )
        kegiatan.save()
        response = self.client.post(reverse('main:daftar',kwargs={'id':kegiatan.pk}), data={
            'daftar':'Fina'
        })
        person = Person.objects.get(nama='Fina')
        count_person_in_kegiatan = kegiatan.person_set.count()
        count_kegiatan_in_person = person.kegiatan.count()
        
        self.assertEqual(count_person_in_kegiatan, 1)
        self.assertEqual(count_kegiatan_in_person, 1)
        self.assertEqual(response.status_code, 302)




# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()




# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
