from django.shortcuts import render, redirect
from .models import Kegiatan, Person
from .forms import KegiatanForm
from datetime import datetime

def home(request):
    data = {
        'kegiatans' : Kegiatan.objects.all(),
        'form' : KegiatanForm,
    }
    return render(request, 'main/home.html', data)

def add_kegiatan(request):
    form = KegiatanForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('main:home')
    return redirect('main:home')

def daftar_kegiatan(request, id):
    if request.method == 'POST':
        kegiatan = Kegiatan.objects.get(id=id)
        person = Person.objects.get_or_create(
            nama=request.POST['daftar']
        )
        person[0].kegiatan.add(kegiatan)
        return redirect('main:home')
    return redirect('main:home')