from django.db import models
from django.utils import timezone
# Create your models here.
class Kegiatan(models.Model):
    title = models.CharField(max_length=30)
    desc = models.CharField(max_length=255, default='')
    date = models.DateTimeField(default=timezone.now, blank=True)
    class Meta:
        # Pengurutan berdasarkan judul
        ordering = ['date']
    
    def __str__(self):
        return self.title

class Person(models.Model):
    nama = models.CharField(max_length=100)
    kegiatan = models.ManyToManyField(Kegiatan)

    class Meta:
        # Pengurutan berdasarkan nama
        ordering = ['nama']

    def __str__(self):
        return self.nama